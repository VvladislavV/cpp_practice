// One definition rule - правило одного определения обеспечивается в общем случае header guards
// header guards будут изучаться при изучении препроцессора
#include "header.hpp"
#include "header.hpp" // поскольку header.hpp содержит header guard, повторное включение будет игнорировано препроцессором

int main()
{
 return 0;
}
