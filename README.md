Первым делом следует прочесть [инструкцию из данного проекта](https://gitlab.com/alexeit/cpp_practice/tree/master/user_manual_gitlab_environment) и выполнить все указанные в ней действия.


Войти в каждый каталог и выполнить 
> ./build.sh

ВНИМАНИЕ:
Для работы с [git_example](https://gitlab.com/alexeit/cpp_practice/tree/master/git_example) скопировать этот каталог ВНЕ данного дерева каталогов, выполнить в этой копии:
> git init

и выполнить
> ./build.sh


# Путеводитель по материалам практики

* [Форматирование вывода с использование setw(), локали и кириллицы в Unicode](https://gitlab.com/alexeit/cpp_practice/tree/master/wide_unicode_io_locale)
* [Литералы, определяемые пользователем](https://gitlab.com/alexeit/cpp_practice/tree/master/user_defined_literals)